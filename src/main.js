// import vue
import Vue from 'vue'
// import Element UI
import ElementUI from 'element-ui'
// Element UI Default Theme
//import 'element-ui/lib/theme-chalk/index.css'

// Custom Theme for SKY
import './element-variables.scss'
// Import App Component
import App from './App.vue'

// Setup vue to use ElementUI
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
